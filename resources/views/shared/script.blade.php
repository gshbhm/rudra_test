<style type="text/css">
    .wrap {height: 100%; }
    .chat { 
        height: 40vh;
        overflow-y: auto}
</style>
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.15.3/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.1/vue.min.js"></script>
<script src="https://www.gstatic.com/firebasejs/3.7.0/firebase.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>

<script>
    $('.reuired-loading').hide();
  // Initialize Firebase
    var user = {!! \Auth::user() !!};
    var id = {{$id}};
    var config = {
        apiKey: "{{ env('firebase_api') }}",
        authDomain: "{{ env('firebase_domain') }}",
        databaseURL: "{{ env('firebase_database') }}",
        storageBucket: "{{ env('firebase_bucket') }}",
        messagingSenderId: "{{ env('firebase_sender_id') }}"
    };
    
    firebase.initializeApp(config);
    let db_name = "";
    if(id < user.id){
        db_name = id+'-'+user.id;  
    }
    else{
        db_name = user.id+'-'+id;
    }
    var database = firebase.database().ref('chats/'+db_name);//.push();
    var message_arr= [];
    database.on('child_added', function(data) {
        $('.reuired-loading').show();
        message_arr.push([data.val().user.name,data.val().text]);
        $('.message-box').append('<div><b>'+data.val().user.name+'</b><p>'+data.val().text+'</p></div>');
    });

    function writeData(text) {
      database.push({
        text: text,
        timestamp: Date.now(),
        user: user
      });
    };



    
    $(document).on('click','.sub-btn',function(){
        if ($('.text-box').val()) {
            writeData($('.text-box').val());
            $('.text-box').val("");    
        }else{
            alert("textbox can't be empty");
        }
        
    });

    $(document).on('click','.export-chat',function(){
        
        let csvContent = "data:text/csv;charset=utf-8," 
            + message_arr.map(e => e.join(",")).join("\n");
        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "my_data.csv");
        document.body.appendChild(link);

        link.click();
        
    });

</script>
