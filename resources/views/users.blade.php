
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">&nbsp;</div>

                @foreach($users as $key=>$value)
                    <p><a href="{{url('chat').'/'.$value->id}}">{{$value->name}}</a></p>
                @endforeach
            </div>
        </div>
    </div>
</div>



@endsection
