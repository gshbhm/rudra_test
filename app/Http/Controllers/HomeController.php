<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::whereNotIn('id',[\Auth::user()->id])->get();
        return view('users', compact('users'));
    }

    public function chat($id)
    {
        $user = \Auth::user();

        return view('home', compact('user','id'));
    }
}
